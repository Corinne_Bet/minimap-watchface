#ifndef WATCHY_MINIMAP_H
#define WATCHY_MINIMAP_H

#include <Watchy.h>
#include "minimap.h"

class WatchyMinimap : public Watchy{
    using Watchy::Watchy;
    public:
      int positionTime(char returnValue, int value, int minValue, int maxValue){
        int x = 82;
        int y = 2;
        int positions[] = {0,0};
        int newValue = map(value, minValue, maxValue, 0, 652);
        if (newValue < 82){
          x = x + newValue;
          y = y;
        } else if (newValue >= 82 && newValue < 245){
          x = 163;
          y = y + (newValue - 82);
        } else if (newValue >= 245 && newValue < 408){
          x = 163 - (newValue-245);
          y = 163;
        } else if (newValue >= 408 && newValue < 571){
          x = 0;
          y = 163 - (newValue - 408);
        }else if (newValue >= 571 && newValue < 653){
          x = 0 + (newValue - 571);
          y = y;
        };
        positions[0] = x;
        positions[1] = y;

        if (returnValue == 'x'){
          return positions[0];
        } else if (returnValue == 'y'){
          return positions[1];
        };
      };
        void drawWatchFace();
};

#endif