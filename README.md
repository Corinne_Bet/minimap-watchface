# Minimap Watchface

![Preview of Minimap watchface](src/Preview.png)

A watchface for the watchy [watch by sqfmi](https://watchy.sqfmi.com/) inspired by GTA videogame Minimap.
You can read time by checking "H" (for Hour) and "Mi" (for Minute) position like an analogue watch.
Battery level is visible on left side like life bar in the original videogame.

I used [Dither It](https://ditherit.com/) to dither images and [imgtocpp](https://javl.github.io/image2cpp/) to convert in array data.

You cand find in src folder all images used (non dithered).

## Authors and acknowledgment
A Corinne Bet project

## License
Licensed as CC-BY-NC

