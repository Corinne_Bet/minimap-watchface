#include "Watchy_Minimap.h"

//const unsigned char *numbers [10] = {numbers0, numbers1, numbers2, numbers3, numbers4, numbers5, numbers6, numbers7, numbers8, numbers9};


void WatchyMinimap::drawWatchFace(){
    int hour = currentTime.Hour;
    if (hour > 12){
      hour = hour - 12;
    };
    int minute = currentTime.Minute;

    int posHX = positionTime('x',hour,0,12);
    int posHY = positionTime('y',hour,0,12);
    int posMiX = positionTime('x',minute,0,59);
    int posMiY = positionTime('y',minute,0,59);

    display.fillScreen(GxEPD_WHITE);
    //battery display (UPCOMING)
    float roundBAT = getBatteryVoltage() * 100;
    float VBAT = map(roundBAT,380,450,400,0);
    //float VBAT = 150;
    for (int i = 2 ; i < VBAT ; i++){
      if (i < 100){
        display.drawLine(100, 100, 100-i, 0, GxEPD_BLACK);
      } else if (i >= 100 && i < 300){
        display.drawLine(100, 100, 0, i-100, GxEPD_BLACK);
      } else if (i >= 300 && i <= 400){
        display.drawLine(100, 100, i-300, 200, GxEPD_BLACK);
      };
    };
    display.drawBitmap(21, 21, MaskInside, 157, 157, GxEPD_WHITE);
    display.drawBitmap(0, 0, MaskOutside, 100, 200, GxEPD_WHITE);
    display.drawBitmap(12, 12, window, 175, 175, GxEPD_BLACK);
    display.drawBitmap(87, 84, BackPlayerIcon, 26, 32, GxEPD_WHITE);
    display.drawBitmap(87, 84, PlayerIcon, 26, 32, GxEPD_BLACK);
    display.drawBitmap(posHX+2, posHY, BackH, 24, 31, GxEPD_WHITE);
    display.drawBitmap(posHX+2, posHY, H, 24, 31, GxEPD_BLACK);
    display.drawBitmap(0, 83, BackNorth, 34, 34, GxEPD_WHITE);
    display.drawBitmap(0, 83, North, 34, 34, GxEPD_BLACK);
    display.drawBitmap(posMiX, posMiY, BackMi, 38, 30, GxEPD_WHITE);
    display.drawBitmap(posMiX, posMiY, Mi, 38, 30, GxEPD_BLACK);
    display.setTextColor(GxEPD_BLACK);
    display.setCursor(10, 170);
    /*display.fillRect(5,165,120,15,GxEPD_WHITE);
    display.print(VBAT);
    int batteryPercent = map(getBatteryVoltage(), 3.2, 4.1, 0, 100);
    if(currentTime.Hour < 10){
        display.print('0');
    };
    display.print(currentTime.Hour);
    display.print(':');
    if(currentTime.Minute < 10){
        display.print('0');
    };
    display.print(currentTime.Minute);
    display.print('-');
    display.print(hour);
    display.print('-');
    display.print(posHX);
    display.print('-');
    display.print(posHY);*/
    
    //display.drawRoundRect(int16_t int 12, int16_t int 12, int16_t int 175, int16_t int 175, int16_t int 5, uint16_t GxEPD_BLACK);
    //display.fillRect(20,140,160,50,GxEPD_WHITE);

    //Hour
    //display.drawBitmap(25, 70, numbers[currentTime.Hour/10], 38, 50, GxEPD_BLACK); //first digit
    //display.drawBitmap(60, 70, numbers[currentTime.Hour%10], 38, 50, GxEPD_BLACK); //second digit

    //Colon
    //display.drawBitmap(90, 80, colon, 11, 31, GxEPD_BLACK); //second digit

    //Minute
    //display.drawBitmap(105, 70, numbers[currentTime.Minute/10], 38, 50, GxEPD_BLACK); //first digit
    //display.drawBitmap(143, 70, numbers[currentTime.Minute%10], 38, 50, GxEPD_BLACK); //second digit
};



